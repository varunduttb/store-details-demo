package com.alfaromeo.annotation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * Created by maverick on 14/1/18.
 * Not needed apparently
 * krishna first commit
 */
//@Component
public class AnnotationProcessor implements BeanPostProcessor {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /*@Autowired
    private JdbcMutableAclService aclService;*/

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {

        /*if (bean.getClass().isAnnotationPresent(Protected.class)) {
            try {
            String insertClass = null // get it from mutableAclService
            jdbcTemplate.update(insertClass, bean.getClass().getCanonicalName());
            } catch (Exception e) {
                logger.warn(e.getLocalizedMessage(), e);
            }
        }*/
        return bean;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }
}
