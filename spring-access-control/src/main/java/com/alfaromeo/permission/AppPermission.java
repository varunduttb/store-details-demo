package com.alfaromeo.permission;

import org.springframework.security.acls.domain.AbstractPermission;

/**
 * Created by Varun dutt on 9/1/18.
 */
public class AppPermission extends AbstractPermission {

    private static final long serialVersionUID = 1L;

    public AppPermission(int mask, String name) {
        super(mask);
        this.mask = mask;
        this.name = name;
    }

    public AppPermission(int mask) {
        super(mask);
        this.mask = mask;
    }

    protected AppPermission(int mask, char code) {
        super(mask, code);
    }

    private String name;

    private int mask;

    private char code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMask(int mask) {
        this.mask = mask;
    }
}
