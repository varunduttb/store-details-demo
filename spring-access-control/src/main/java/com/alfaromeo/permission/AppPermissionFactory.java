package com.alfaromeo.permission;

import com.alfaromeo.configuration.RoleConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.domain.DefaultPermissionFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by {@link org.springframework.security.acls.domain.PermissionFactory} on 9/1/18.
 * implementation adds permission defined into the ACL context
 */

@Component
public class AppPermissionFactory extends DefaultPermissionFactory {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RoleConfiguration configuration;

    private int mask = 32;

    public AppPermissionFactory() {
        super();
    }

    public void initFactory(AppPermission permission) {
        registerPermission(permission, permission.getName());
    }
    @PostConstruct
    public void init() {
        Set<String> uniquePermissions = new HashSet<>();
        configuration.getRoles().stream().forEach(roleDefinition -> {
            uniquePermissions.addAll(roleDefinition.getDomainPermissions());
        });
        uniquePermissions.stream().forEach(permission -> {
            try {
                buildFromName(permission);
            } catch (IllegalArgumentException e) {
                initFactory(new AppPermission(mask, permission));
                mask=mask*2;
            }
        });
    }

}
