package com.alfaromeo.service.impl;

import com.alfaromeo.service.IAclService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.domain.ObjectIdentityImpl;
import org.springframework.security.acls.domain.PermissionFactory;
import org.springframework.security.acls.domain.PrincipalSid;
import org.springframework.security.acls.model.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Varun dutt on 9/1/18.
 */

@Service
public class AclServiceImpl implements IAclService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MutableAclService aclService;

    @Autowired
    private PermissionFactory permissionFactory;

    @Override
    @Transactional
    public void grantPermissionsToUser(Class<?> clazz, Set<Serializable> entityIds, List<String> permissions,
                                       String username) {
        //Prepare information needed in the access control entries(ACEs)
        List<Permission> permissionSet = permissionFactory.buildFromNames(permissions);
        Sid sid = new PrincipalSid(username);
        List<ObjectIdentity> objectIdentities = entityIds.stream().map(id ->
                new ObjectIdentityImpl(clazz, id)).collect(Collectors.toList());

        //Create/Update the relevant ACLs
        List<MutableAcl> acls = this.getAcls(objectIdentities);

        //Grant permissions via ACEs
        acls.stream().forEach(acl -> {
            permissionSet.stream().forEach(permission -> {
                acl.insertAce(acl.getEntries().size(), permission, sid, true);
            });
            aclService.updateAcl(acl);
        });
    }

    @Override
    public void revokePermissionsFromUser(Class<?> clazz, Set<Serializable> entityIds, List<String> permissions,
                                          String username) {
        //Prepare information needed in the access control entries(ACEs)
        List<Permission> permissionSet = permissionFactory.buildFromNames(permissions);
        Sid sid = new PrincipalSid(username);
        List<ObjectIdentity> objectIdentities = entityIds.stream().map(id ->
                new ObjectIdentityImpl(clazz, id)).collect(Collectors.toList());

        //Create/Update the relevant ACLs
        List<MutableAcl> acls = this.getAcls(objectIdentities);

        //Revoke permissions via ACEs
        acls.stream().forEach(acl -> {
            permissionSet.stream().forEach(permission -> {
                acl.getEntries().stream().filter(entry -> entry
                        .getPermission().equals(permission) && entry.getSid().equals(sid))
                        .forEach(accessControlEntry -> {
                            acl.deleteAce(acl.getEntries().indexOf(accessControlEntry));
                        });
            });
            aclService.updateAcl(acl);
        });
    }

    @Override
    public void deleteAcls(Class<?> clazz, Set<Serializable> entityIds) {
        List<ObjectIdentity> objectIdentities = entityIds.stream().map(id ->
                new ObjectIdentityImpl(clazz, id)).collect(Collectors.toList());
        //Create/Update the relevant ACLs
        objectIdentities.stream().forEach(oid -> {
            aclService.deleteAcl(oid, false);
        });
    }

    private List<MutableAcl> getAcls(List<ObjectIdentity> objectIdentities) {
        return objectIdentities.stream().map(objectIdentity -> {
            MutableAcl acl;
            try {
                acl = (MutableAcl) aclService.readAclById(objectIdentity);
            } catch (NotFoundException e) {
                logger.info(e.getMessage());
                acl = aclService.createAcl(objectIdentity);
            }
            return acl;
        }).collect(Collectors.toList());
    }

}
