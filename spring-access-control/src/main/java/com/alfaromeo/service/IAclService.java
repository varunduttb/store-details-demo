package com.alfaromeo.service;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * Created by Varun dutt on 9/1/18.
 * This class will enable mutations of ACLs
 */
public interface IAclService {

    void grantPermissionsToUser(Class<?> clazz, Set<Serializable> entityIds,
                                List<String> permissions, String username);

    void revokePermissionsFromUser(Class<?> clazz, Set<Serializable> entityIds,
                                   List<String> permissions, String username);

    void deleteAcls(Class<?> clazz, Set<Serializable> entityIds);



}
