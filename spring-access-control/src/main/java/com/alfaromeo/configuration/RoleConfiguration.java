package com.alfaromeo.configuration;

import com.alfaromeo.model.RoleDefinition;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by maverick on 14/1/18.
 */
@Component
@ConfigurationProperties
public class RoleConfiguration {

    private List<RoleDefinition> roles;

    public List<RoleDefinition> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleDefinition> roles) {
        this.roles = roles;
    }
}
