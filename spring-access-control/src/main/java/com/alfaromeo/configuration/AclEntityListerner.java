package com.alfaromeo.configuration;

import com.alfaromeo.annotation.Protected;
import com.alfaromeo.service.IAclService;
import com.alfaromeo.utils.AclUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.util.ReflectionUtils;

import javax.persistence.Id;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by maverick on 14/1/18.
 */
@Component
public class AclEntityListerner implements ApplicationContextAware {

    private static IAclService aclservice;

    private Logger logger = LoggerFactory.getLogger(AclEntityListerner.class);

    public AclEntityListerner(IAclService aclservice) {
        aclservice = aclservice;
    }

    @PostPersist
    public void onPostPersist(Object entity) {
        this.aclTransactions(entity, true);
    }

    @PostRemove
    public void onPostRemove(Object entity) {
        this.aclTransactions(entity, false);
    }

    private void aclTransactions(Object entity, boolean isPersist) {
        if (securityContextPresent() &&
                entity.getClass().isAnnotationPresent(Protected.class)) {
            ReflectionUtils.doWithLocalFields(entity.getClass(), field -> {
                if (field.isAnnotationPresent(Id.class)) {
                    TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronization() {
                        @Override
                        public void afterCommit() {
                            String username = AclUtilities.getLoggedInUsername();
                            try {
                                if (isPersist) {
                                    aclservice.grantPermissionsToUser(entity.getClass(),
                                            Stream.of((Long)field.get(entity)).collect(Collectors.toSet()),
                                            getPermissions(),
                                            username);
                                } else {
                                    aclservice.deleteAcls(entity.getClass(),
                                            Stream.of((Long)field.get(entity)).collect(Collectors.toSet()));
                                }
                            } catch (IllegalAccessException e) {
                                logger.error(e.getLocalizedMessage(), e);
                            }
                        }
                    });

                }
            });
        }
    }

    private boolean securityContextPresent() {
        return SecurityContextHolder.getContext().getAuthentication()!=null;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        aclservice = applicationContext.getBean(IAclService.class);
    }

    private List<String> getPermissions() {
        return Stream.of("READ").collect(Collectors.toList());
    }
}
