package com.alfaromeo.model;

import java.util.Set;

/**
 * Created by maverick on 14/1/18.
 */
public class RoleDefinition {

    private String name;

    private Set<String> domainPermissions;

    private Set<String> methodPermissions;

    public Set<String> getDomainPermissions() {
        return domainPermissions;
    }

    public void setDomainPermissions(Set<String> domainPermissions) {
        this.domainPermissions = domainPermissions;
    }

    public Set<String> getMethodPermissions() {
        return methodPermissions;
    }

    public void setMethodPermissions(Set<String> methodPermissions) {
        this.methodPermissions = methodPermissions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
