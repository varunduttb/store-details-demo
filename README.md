 This is a multi module Maven project with the following modules which are integrated with eachother
 1. **store-demo** - demonstrates the relationships between the entities *User, Store and Product*
 2. **spring-security-starter** - Enables user login through database Authentication
 
#### Entity relationships
* LFSStore table - Will have store related metadata
* Product table - All the product related metadada
* ProductDetails - Product data specific to a store, will be mapped with LFS store and Product tables
* User table - Each LFS store will be mapped to multiple Users as CROs

![ERD Diagram](erd.png) 



:warning: *store-demo* is protected using *spring-security-starter* module using the following steps
* Added the dependency in *pom.xml* of *store-demo*

```xml
        <dependency>
			<groupId>com.alfaromeo</groupId>
			<artifactId>spring-security-starter</artifactId>
			<version>0.0.1</version>
		</dependency>
```

* Added this in *application.yml*

```yml
jwt:
  # We need to use a public-private key in production
  secret: some-secret
  #token valid for 10 hours, time in seconds
  expiration: 36000
```


 
 
### Pre-requisities
---------------------------------------------------
mysql database with name *store*

### Usage
--------------------------------------------------- 
 *store-demo* is a spring boot application, so start it as one. It starts on *8000*.
 
 User login part is same as our previous application - like [this](https://gitlab.com/varunduttb/spring-security-starter#using-the-application)
 
 for Rest Api documentation - http://localhost:8000/swagger-ui

:notebook: Please remove the *utils* package in store-demo project while moving
the code to production, the initial data is too complex to be populated through 
*schema.sql* scripts.