package com.alfaromeo.common.controller;

import com.alfaromeo.common.domain.User;
import com.alfaromeo.common.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Varun dutt
 * {@link RestController} for User service
 */

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private IUserService userservice;

    @PostMapping
    @ResponseBody
    public User createUser(@RequestBody User user) {
        return userservice.createUser(user);
    }

    @PostMapping("/list")
    @ResponseBody
    public List<User> createUsers(@RequestBody List<User> users) {
        return userservice.createUsers(users);
    }

    @PutMapping
    @ResponseBody
    public User updateUser(@RequestBody User user) {
        return userservice.updateUser(user);
    }

    @DeleteMapping("/{id:\\d+}")
    @ResponseBody
    public void deleteUser(@PathVariable Long id) {
        userservice.deleteUser(id);
    }

    @DeleteMapping
    @ResponseBody
    public void deleteUsers() {
        userservice.deleteUsers();
    }

    @GetMapping
    @ResponseBody
    public List<User> getUsers() {
        return userservice.getUsers();
    }

    @GetMapping("/{id:\\d+}")
    @ResponseBody
    public User getUserById(@PathVariable Long id) {
        return userservice.getUser(id);
    }

    @GetMapping("/username/{username:.+}")
    @ResponseBody
    public UserDetails getUserByUsername(@PathVariable String username) {
        return userservice.loadUserByUsername(username);
    }
}
