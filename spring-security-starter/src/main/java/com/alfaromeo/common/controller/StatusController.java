package com.alfaromeo.common.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * This class in a Rest Controller which has the status end point
 * @author Varun dutt
 */
@RestController
@RequestMapping("/status")
public class StatusController {

    @GetMapping
    @ResponseBody
    public String status() {
        return "Alive at " + new Date() + " serving your requests";
    }
}
