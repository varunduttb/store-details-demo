package com.alfaromeo.common.model;

/**
 * Created by maverick on 14/1/18.
 */
public enum TargetType {

    USER, ROLE
}
