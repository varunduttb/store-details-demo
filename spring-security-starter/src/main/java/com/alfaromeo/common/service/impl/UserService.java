package com.alfaromeo.common.service.impl;

import com.alfaromeo.common.domain.User;
import com.alfaromeo.common.repository.UserRepository;
import com.alfaromeo.common.service.IUserService;
import com.alfaromeo.common.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * @author Varun dutt
 */
@Service
public class UserService implements IUserService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> createUsers(List<User> users) {
        return userRepository.saveAll(users);
    }

    @Override
    public User createUser(User user) {
        user.setCreatedBy(Utils.getLoggedInUsername());
        user.setModifiedBy(Utils.getLoggedInUsername());
        return userRepository.save(user);
    }

    @Override
    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @Override
    public User getUser(Long id) {
        try {
            return userRepository.findById(id).get();
        } catch (NoSuchElementException e) {
            logger.error(e.getLocalizedMessage(), e);
            throw e;
        }
    }

    @Override
    public User updateUser(User user) {
        if (user.getId() == null) {
            throw new EntityNotFoundException("ID cannot be null");
        }
        user.setModifiedBy(Utils.getLoggedInUsername());
        return userRepository.save(user);
    }

    @Override
    public void deleteUsers() {
        userRepository.deleteAllInBatch();
    }

    @Override
    public void deleteUsers(Long[] idArray) {
        List<Long> ids = new ArrayList(Arrays.asList(idArray));
        List<User> users = userRepository.findAllById(ids);
        userRepository.deleteInBatch(users);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            return userRepository.findByUsername(username);
        } catch (UsernameNotFoundException e) {
            logger.error(e.getLocalizedMessage(), e);
            throw e;
        }
    }
}
