package com.alfaromeo.common.service;

import com.alfaromeo.common.domain.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

/**
 * @author Varun dutt
 */
public interface IUserService extends UserDetailsService {

    List<User> createUsers(List<User> users);

    User createUser(User user);

    void deleteUser(Long id);

    List<User> getUsers();

    User getUser(Long id);

    User updateUser(User user);

    void deleteUsers();

    void deleteUsers(Long[] ids);
}
