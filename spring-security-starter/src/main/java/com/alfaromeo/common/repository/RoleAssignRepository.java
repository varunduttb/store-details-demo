package com.alfaromeo.common.repository;

import com.alfaromeo.common.domain.RoleAssignment;
import com.alfaromeo.common.model.TargetType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by maverick on 14/1/18.
 */
@Repository
public interface RoleAssignRepository extends JpaRepository {

    Optional<RoleAssignment> findByTargetAndRoleAndTargetType(String target,
                                                              String role,
                                                              TargetType targetType);

    List<RoleAssignment> findByTargetAndTargetType(String target,
                                                   TargetType targetType);
}
