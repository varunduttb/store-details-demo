package com.alfaromeo.common.domain;

import com.alfaromeo.common.model.TargetType;

import javax.persistence.*;

/**
 * Created by maverick on 14/1/18.
 */
@Entity
@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"target", "role", "target_type"})
})
public class RoleAssignment extends Metadata {

    @Column(nullable = false)
    private String target;

    @Column(nullable = false)
    private String role;

    @Enumerated(EnumType.STRING)
    @Column(name = "target_type")
    private TargetType targetType;

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public TargetType getTargetType() {
        return targetType;
    }

    public void setTargetType(TargetType targetType) {
        this.targetType = targetType;
    }
}
