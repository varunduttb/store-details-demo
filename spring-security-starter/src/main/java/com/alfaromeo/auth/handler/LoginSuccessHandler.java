package com.alfaromeo.auth.handler;

import com.alfaromeo.auth.model.AuthenticationResponse;
import com.alfaromeo.auth.service.impl.TokenAuthenticationService;
import com.alfaromeo.common.domain.RoleAssignment;
import com.alfaromeo.common.domain.User;
import com.alfaromeo.common.model.TargetType;
import com.alfaromeo.common.repository.RoleAssignRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.stream.Collectors;

/**
 * On successful Authentication, onAuthenticationSuccess is called,
 * we issue a user token for the logged in user which he can use in the subsequent requests
 * @author Varun dutt
 */
@Component
public class LoginSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private TokenAuthenticationService tokenAuthenticationService;

    @Autowired
    private MappingJackson2HttpMessageConverter messageConverter;

    @Autowired
    private RoleAssignRepository roleAssignRepository;

    private ObjectMapper mapper = new ObjectMapper();

    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        Assert.notNull(authentication, "Authentication for a successfully logged in user should not be null.");
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.setStatus(HttpServletResponse.SC_OK);

        UserDetails authenticatedUser = (UserDetails) authentication.getPrincipal();

        if (authenticatedUser != null) {
            logger.info("User {} has been successfully authenticated", authenticatedUser);

            this.addAuthorities(authenticatedUser);
            String token = tokenAuthenticationService.generateToken(authenticatedUser);

            PrintWriter writer = response.getWriter();
            AuthenticationResponse authenticationResponse = new AuthenticationResponse();
            authenticationResponse.setUserToken(token);
            mapper = messageConverter.getObjectMapper();
            mapper.writeValue(writer, authenticationResponse);

            writer.flush();
            writer.close();

            clearAuthenticationAttributes(request);
        } else {
            throw new AuthenticationCredentialsNotFoundException("Authenticated User is null, which is a complete nonsense");
        }
    }

    private void addAuthorities(UserDetails authenticatedUser) {
        List<RoleAssignment> roleAssignments = roleAssignRepository
                .findByTargetAndTargetType(authenticatedUser.getUsername(),
                        TargetType.USER);
        List<GrantedAuthority> authorities = roleAssignments.stream()
                .map(roleAssignment -> new SimpleGrantedAuthority(roleAssignment.getTarget()))
                .collect(Collectors.toList());
        ((User)authenticatedUser).setAuthorities(authorities);
    }

}
