package com.alfaromeo.auth.handler;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.*;

/**
 * This class handles generation and validation of JWT tokens
 * @author Varun dutt
 */
@Component
public class TokenHandler implements Serializable {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String CLAIM_KEY_USERNAME = "sub";
    private static final String CLAIM_KEY_CREATED = "created";
    private static final String CLAIM_KEY_AUTHORITIES = "authorities";

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expiration}")
    private Long expiration;

    private Claims getClaimsFromToken(String token) {
        Claims claims;
        try {
            claims = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage(), e);
            throw e;
        }
        return claims;
    }

    public String getUsernameFromToken(String token) {
        String username;
        try {
            final Claims claims = getClaimsFromToken(token);
            username = claims.getSubject();
        } catch (JwtException e) {
            logger.error(e.getLocalizedMessage(), e);
            throw e;
        }
        return username;
    }

    private Date getExpiryDateOfToken(String token) {
        Date expiryDate;
        try {
            final Claims claims = getClaimsFromToken(token);
            expiryDate = claims.getExpiration();
        } catch (JwtException e) {
            logger.error(e.getLocalizedMessage(), e);
            throw e;
        }
        return expiryDate;
    }

    public Date getCreatedDateFromToken(String token) {
        Date created;
        try {
            final Claims claims = getClaimsFromToken(token);
            created = new Date((Long) claims.get(CLAIM_KEY_CREATED));
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage(), e);
            created = null;
        }
        return created;
    }

    public List<GrantedAuthority> getAuthoritiesFromToken(String token) {
        List<GrantedAuthority> authorities = new ArrayList();
        try {
            final Claims claims = getClaimsFromToken(token);
            List<LinkedHashMap> authorityList = claims.get(CLAIM_KEY_AUTHORITIES, List.class);
            authorityList.stream().forEach(authorityMap -> {
                authorities.add(new SimpleGrantedAuthority(authorityMap.get("authority").toString()));
            });
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage(), e);
            throw e;
        }
        return authorities;
    }

    public String generateToken(UserDetails user) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(CLAIM_KEY_USERNAME, user.getUsername());
        claims.put(CLAIM_KEY_CREATED, new Date());
        claims.put(CLAIM_KEY_AUTHORITIES, user.getAuthorities());
        return generateToken(claims);
    }

    private String generateToken(Map<String, Object> claims) {
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(generateExpirationDate())
                .setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    private Date generateExpirationDate() {
        return new Date(System.currentTimeMillis() + expiration * 1000);
    }

    public Boolean isTokenExpired(String token) {
        return getExpiryDateOfToken(token).before(new Date());
    }
}
