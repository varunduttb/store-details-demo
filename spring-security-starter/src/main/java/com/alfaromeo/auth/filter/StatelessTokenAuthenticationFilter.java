package com.alfaromeo.auth.filter;

import com.alfaromeo.auth.service.ITokenAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This Filter captures all the incoming requests {except for logins as they are captured
 * in the other filter added before}
 * @author Varun dutt
 */
@Service
public class StatelessTokenAuthenticationFilter extends GenericFilterBean {

    @Autowired
    private ITokenAuthenticationService tokenAuthenticationService;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            HttpServletRequest httpRequest = ((HttpServletRequest)request);
            if (httpRequest.getHeader("Authorization") != null
                    && StringUtils.startsWithIgnoreCase(httpRequest.getHeader("Authorization"), "Bearer")) {
                Authentication authentication = tokenAuthenticationService.getAuthentication((HttpServletRequest)request, (HttpServletResponse) response);
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
            chain.doFilter(request, response);
        } finally {
            SecurityContextHolder.clearContext();
        }
    }
}
