package com.alfaromeo.auth.filter;

import com.alfaromeo.auth.model.AuthenticationRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This class captures incoming Login requests and authenticate the requests
 * with the {@link org.springframework.security.authentication.AuthenticationManager}
 * @author Varun dutt
 */
public class StatelessAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private ObjectMapper objectMapper = new ObjectMapper();

    public StatelessAuthenticationFilter(String defaultFilterProcessesUrl) {
        super(defaultFilterProcessesUrl);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws AuthenticationException, IOException, ServletException {
        final AuthenticationRequest request = objectMapper
                .readValue(httpServletRequest.getInputStream(), AuthenticationRequest.class);
        if (request == null) throw new NullPointerException("Authentication request cannot be null");
        Authentication authentication = new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword());

        Authentication authenticatedUser;
        try {
            authenticatedUser = getAuthenticationManager().authenticate(authentication);
        } catch (AuthenticationException e) {
            logger.error(e.getLocalizedMessage(), e);
            throw e;
        }
        return authenticatedUser;
    }
}
