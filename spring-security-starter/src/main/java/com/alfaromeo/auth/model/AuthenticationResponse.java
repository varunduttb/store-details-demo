package com.alfaromeo.auth.model;

public class AuthenticationResponse {

	private String userToken;

	public String getUserToken() {
		return userToken;
	}

	public void setUserToken(String userToken) {
		this.userToken = userToken;
	}
}
