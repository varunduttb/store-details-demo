package com.alfaromeo.auth.service;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Varun dutt
 */

public interface ITokenAuthenticationService {

	String generateToken(UserDetails userDetails);

	Authentication getAuthentication(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
}
