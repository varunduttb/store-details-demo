package com.alfaromeo.auth.service.impl;

import com.alfaromeo.auth.handler.TokenHandler;
import com.alfaromeo.auth.service.ITokenAuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * This class handles token based authentication,
 * extracts the Authorization header from the incoming non-login
 * requests and validate the token {The header value} and let requests proceed
 * only if the tokens are valid
 * @author Varun dutt
 */
@Service
public class TokenAuthenticationService implements ITokenAuthenticationService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private final static String AUTHORIZATION_HEADER = "Authorization";

    @Autowired
    private TokenHandler tokenHandler;

    @Override
    public String generateToken(UserDetails userDetails) {
        String token = tokenHandler.generateToken(userDetails);
        return token;
    }

    @Override
    public Authentication getAuthentication(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Authentication authentication;
        try {
            authentication = SecurityContextHolder.getContext().getAuthentication();

            String authorizationHeader = request.getHeader(AUTHORIZATION_HEADER);

            final String token = authorizationHeader.substring(7);

            final String username = tokenHandler.getUsernameFromToken(token);
            final List<GrantedAuthority> authorities = tokenHandler.getAuthoritiesFromToken(token);
            if (authentication == null) {
                // Password is sent null as we won't be having it at this stage
                // When you don't send the password null, 403 Forbidden is thrown as it sets isAuthenticated() to false
                authentication = new UsernamePasswordAuthenticationToken(username, null, authorities);
            }
        } catch (AuthenticationException e) {
            logger.error(e.getLocalizedMessage(), e);
            throw e;
        }
        return authentication;
    }
}
