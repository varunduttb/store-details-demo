package com.alfaromeo.auth.configuration;

import com.alfaromeo.auth.filter.StatelessAuthenticationFilter;
import com.alfaromeo.auth.filter.StatelessTokenAuthenticationFilter;
import com.alfaromeo.auth.handler.LoginFailureHandler;
import com.alfaromeo.auth.handler.LoginSuccessHandler;
import com.alfaromeo.auth.service.ITokenAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.Filter;
import java.util.ArrayList;
import java.util.List;

/**
 * This class configures Web security for the application,
 * configures the protected and unprotected routes, success & failure handlers,
 *  it also configures the {@link AuthenticationManager} with its {@link List} of {@link AuthenticationProvider}
 * @author Varun dutt
 */
@Configuration
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private DaoAuthenticationProvider daoAuthenticationProvider;

    @Autowired
    private LoginSuccessHandler loginSuccessHandler;

    @Autowired
    private LoginFailureHandler loginFailureHandler;

    @Autowired
    private ITokenAuthenticationService tokenAuthenticationService;

    @Autowired
    private StatelessTokenAuthenticationFilter authenticationFilter;

    private static final String LOGIN_URL = "/auth/login";

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.csrf().disable()
                .authorizeRequests()
                //These REST APIs are provided by default by Spring security
                .antMatchers(LOGIN_URL, "/auth/logout", "/v2/api-docs", "/swagger-resources/**", "/configuration/security", "/swagger-ui.html", "/webjars/**")
                .permitAll()
                .anyRequest().authenticated()
                .and()
                // Adding this filter to capture ONLY login requests
                .addFilterBefore(loginfilter(), UsernamePasswordAuthenticationFilter.class)
                // Adding this filter to handle token based Authentication
                .addFilterBefore(authenticationFilter, UsernamePasswordAuthenticationFilter.class)
                .sessionManagement()
                // Going Stateless as Scaling could be a Concern
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .exceptionHandling()
                .authenticationEntryPoint((request, response, authException) -> {
                    response.sendError(401);
                });
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() {
        List<AuthenticationProvider> providers = new ArrayList();
        providers.add(daoAuthenticationProvider);
        AuthenticationManager authenticationManager = new ProviderManager(providers);
        return  authenticationManager;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder builder) {
        builder.parentAuthenticationManager(authenticationManagerBean());
    }

    public Filter loginfilter() throws Exception {
        StatelessAuthenticationFilter statelessAuthenticationFilter = new StatelessAuthenticationFilter(LOGIN_URL);
        statelessAuthenticationFilter.setAuthenticationManager(authenticationManagerBean());
        statelessAuthenticationFilter.setAuthenticationSuccessHandler(loginSuccessHandler);
        statelessAuthenticationFilter.setAuthenticationFailureHandler(loginFailureHandler);
        return statelessAuthenticationFilter;
    }


}
