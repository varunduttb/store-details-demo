package com.alfaromeo.projection;

import com.alfaromeo.model.Area;
import com.alfaromeo.model.City;

/**
 * Created by Varun dutt on 13/1/18.
 * It's a projection of {@link com.alfaromeo.domain.LFSStore}
 * entity helps to fetch whatever fields are necessary
 */
 public interface StoreLite {

     Long getId();

     String getBranch();

     String getLocation();

     String[] getCros();

     Area getArea();

     City getCity();

}
