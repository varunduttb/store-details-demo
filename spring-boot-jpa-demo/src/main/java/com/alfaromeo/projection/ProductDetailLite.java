package com.alfaromeo.projection;

import com.alfaromeo.domain.Product;

/**
 * Created by Varun dutt on 13/1/18.
 * It's a projection of {@link com.alfaromeo.domain.ProductDetail}
 * entity helps to fetch whatever fields are necessary
 */
public interface ProductDetailLite {

    public Product getProduct();

    public Double getDiscount();

    public Integer getTotalNumberInStock();

    public Long getId();
}
