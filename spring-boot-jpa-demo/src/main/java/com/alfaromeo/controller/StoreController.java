package com.alfaromeo.controller;

import com.alfaromeo.domain.LFSStore;
import com.alfaromeo.domain.ProductDetail;
import com.alfaromeo.model.Area;
import com.alfaromeo.model.City;
import com.alfaromeo.projection.ProductDetailLite;
import com.alfaromeo.projection.StoreLite;
import com.alfaromeo.service.IStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by maverick on 11/1/18.
 */
@RestController
@RequestMapping("stores")
public class StoreController {

    @Autowired
    private IStoreService storeService;

    @GetMapping("/{id:\\d+}")
    @ResponseBody
    public LFSStore getStore(@PathVariable Long id) {
        return storeService.getStore(id);
    }

    @GetMapping
    @ResponseBody
    public List<LFSStore> getStores() {
        return storeService.getStores();
    }

    @PostMapping
    @ResponseBody
    public LFSStore createStore(@RequestBody LFSStore store) {
        return storeService.saveStore(store);
    }

    @DeleteMapping("/{id:\\d+}")
    @ResponseBody
    public void deleteStore(@PathVariable Long id) {
        storeService.deleteStore(id);
    }

    @GetMapping("/area/{areaName}")
    @ResponseBody
    public List<StoreLite> getStoresByArea(@PathVariable Area areaName) {
        return storeService.getStoresByArea(areaName);
    }

    @GetMapping("/city/{cityName}")
    @ResponseBody
    public List<StoreLite> getStoresByCity(@PathVariable City cityName) {
        return storeService.getStoresByCity(cityName);
    }

    @GetMapping("/location-like/{locationlike}")
    @ResponseBody
    List<StoreLite> getStoreByNearbyLocation(@PathVariable String locationlike) {
         return storeService.getStoresByNearbyLocation(locationlike);
    }

    @GetMapping("/location/{location}")
    @ResponseBody
    StoreLite getStoreByLocation(@PathVariable String location) {
        return storeService.getStoreByLocation(location);
    }

    @PostMapping("/{storeId:\\d+}/products")
    @ResponseBody
    void addProductToStore(@PathVariable Long storeId, @RequestBody ProductDetail product) {
        storeService.addProductToStore(storeId, product);
    }

    @DeleteMapping("/{storeId:\\d+}/products/{productDetailId:\\d+}")
    @ResponseBody
    void removeProductFromStore(@PathVariable Long storeId, @PathVariable Long productDetailId) {
        storeService.removeProductFromStore(storeId, productDetailId);

    }

    @GetMapping("/{storeId:\\d+}/products/discount/{discount}")
    @ResponseBody
    List<ProductDetailLite> getProductsByDiscount(@PathVariable Long storeId, @PathVariable Double discount) {
        return storeService.getProductsByDiscount(storeId, discount);
    }

    @GetMapping("/{storeId:\\d+}/products/out-of-stock")
    @ResponseBody
    List<ProductDetailLite> getOutOfStockProducts(@PathVariable Long storeId) {
      return storeService.getOutOfStockProducts(storeId);
    }
}
