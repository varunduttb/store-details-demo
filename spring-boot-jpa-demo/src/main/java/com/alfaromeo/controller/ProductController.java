package com.alfaromeo.controller;

import com.alfaromeo.domain.Product;
import com.alfaromeo.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Varun on 19/12/17.
 */
@RestController
@RequestMapping("products")
public class ProductController {

    @Autowired
    private IProductService productService;

    @PostMapping
    @ResponseBody
    public Product createProduct(@RequestBody Product product) {
        return productService.createProduct(product);
    }

    @GetMapping
    @ResponseBody
    public List<Product> getProducts() {
        return productService.getProducts();
    }

    @GetMapping("/{id:\\d+}")
    @ResponseBody
    public Product getProduct(@PathVariable Long id) {
        return productService.getProduct(id);
    }

    @DeleteMapping("/{id:\\d+}")
    @ResponseBody
    public void deleteProduct(@PathVariable Long id) {
        productService.deleteProduct(id);
    }

    @DeleteMapping
    @ResponseBody
    public void deleteProducts() {
        productService.deleteProducts();
    }
}
