package com.alfaromeo.repository;

import com.alfaromeo.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Varun on 19/12/17.
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

}
