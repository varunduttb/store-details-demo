package com.alfaromeo.repository;

import com.alfaromeo.domain.LFSStore;
import com.alfaromeo.domain.ProductDetail;
import com.alfaromeo.projection.ProductDetailLite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by maverick on 27/12/17.
 */
@Repository
public interface ProductDetailRepository extends JpaRepository<ProductDetail, Long> {

    List<ProductDetailLite> findByStoreAndTotalNumberInStock(LFSStore store, Integer stock);

    List<ProductDetailLite> findByStoreAndDiscount(LFSStore store, Double discount);
}
