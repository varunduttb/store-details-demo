package com.alfaromeo.repository;

import com.alfaromeo.domain.LFSStore;
import com.alfaromeo.model.Area;
import com.alfaromeo.model.City;
import com.alfaromeo.projection.StoreLite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by maverick on 27/12/17.
 */
@Repository
public interface StoreRepository extends JpaRepository<LFSStore, Long> {

    List<StoreLite> findByArea(Area area);

    List<StoreLite> findByCity(City city);

    List<StoreLite> findByLocationLike(String location);

    Optional<StoreLite> findByLocation(String location);
}
