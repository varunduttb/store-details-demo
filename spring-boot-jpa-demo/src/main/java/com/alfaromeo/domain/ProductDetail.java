package com.alfaromeo.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.springframework.util.Assert;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by maverick on 23/12/17.
 */
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class ProductDetail implements Serializable {

    public ProductDetail() {}

    public ProductDetail(Product product, Double discount, Integer totalNumberInStock) {
        this.discount = discount;
        this.product = product;
        this.totalNumberInStock = totalNumberInStock;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Product product;

    private Double discount = 0.0;

    @ManyToOne(targetEntity = LFSStore.class)
    private LFSStore store;

    @Column(nullable = false)
    private Integer totalNumberInStock;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        Assert.isTrue(discount<100, "Discount cannot be more than 100");
        this.discount = discount;
    }

    public Integer getTotalNumberInStock() {
        return totalNumberInStock;
    }

    public void setTotalNumberInStock(Integer totalNumberInStock) {
        this.totalNumberInStock = totalNumberInStock;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
