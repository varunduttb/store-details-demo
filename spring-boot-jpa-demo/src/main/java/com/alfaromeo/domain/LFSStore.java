package com.alfaromeo.domain;

import com.alfaromeo.common.domain.User;
import com.alfaromeo.model.Area;
import com.alfaromeo.model.City;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by maverick on 23/12/17.
 */
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class LFSStore implements Serializable {

    public LFSStore() {}

    public LFSStore(String branch, String location,
                    List<ProductDetail> products, List<User> cros, Area area, City city) {
        this.branch = branch;
        this.location = location;
        this.products = products;
        this.cros = cros;
        this.area = area;
        this.city = city;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "area")
    private Area area;

    @Enumerated(EnumType.STRING)
    @Column(name = "city")
    private City city;

    @Column(nullable = false, unique = true)
    private String branch;

    @Column(length = 1000)
    private String location;

    @OneToMany(cascade = {CascadeType.MERGE, CascadeType.REMOVE, CascadeType.REFRESH})
    private List<ProductDetail> products = new ArrayList<>();

    @OneToMany
    private List<User> cros;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<ProductDetail> getProducts() {
        return products;
    }

    public void setProducts(List<ProductDetail> products) {
        this.products = products;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public List<User> getCros() {
        return cros;
    }

    public void setCros(List<User> cros) {
        this.cros = cros;
    }
}
