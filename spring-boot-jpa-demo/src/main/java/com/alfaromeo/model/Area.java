package com.alfaromeo.model;

/**
 * Created by maverick on 11/1/18.
 */
public enum Area {

    NORTH, SOUTH, EAST, WEST
}
