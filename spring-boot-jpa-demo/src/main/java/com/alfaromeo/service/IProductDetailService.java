package com.alfaromeo.service;

import com.alfaromeo.domain.ProductDetail;

import java.util.List;

/**
 * Created by maverick on 27/12/17.
 */
public interface IProductDetailService {

    List<ProductDetail> getProductDetails();

    ProductDetail getProductDetail(Long id);

    void deleteProductDetail(Long id);

    ProductDetail saveProductDetail(ProductDetail productDetail);
}
