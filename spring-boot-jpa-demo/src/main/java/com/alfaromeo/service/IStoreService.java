package com.alfaromeo.service;

import com.alfaromeo.domain.LFSStore;
import com.alfaromeo.domain.ProductDetail;
import com.alfaromeo.model.Area;
import com.alfaromeo.model.City;
import com.alfaromeo.projection.ProductDetailLite;
import com.alfaromeo.projection.StoreLite;

import java.util.List;

/**
 * Created by maverick on 27/12/17.
 */
public interface IStoreService {

    List<LFSStore> getStores();

    LFSStore getStore(Long id);

    void deleteStore(Long id);

    LFSStore saveStore(LFSStore store);

    List<StoreLite> getStoresByArea(Area area);

    List<StoreLite> getStoresByCity(City city);

    List<StoreLite> getStoresByNearbyLocation(String locationlike);

    StoreLite getStoreByLocation(String location);

    void addProductToStore(Long storeId, ProductDetail product);

    void removeProductFromStore(Long storeId, Long productDetailId);

    List<ProductDetailLite> getProductsByDiscount(Long storeId, Double discount);

    List<ProductDetailLite> getOutOfStockProducts(Long storeId);

}
