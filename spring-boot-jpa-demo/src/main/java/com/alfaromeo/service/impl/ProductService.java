package com.alfaromeo.service.impl;

import com.alfaromeo.domain.Product;
import com.alfaromeo.repository.ProductRepository;
import com.alfaromeo.service.IProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by Varun on 20/12/17.
 */
@Service
public class ProductService implements IProductService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ProductRepository productRepository;

    public Product createProduct(Product product) {
        return productRepository.save(product);
    }

    public List<Product> getProducts() {
        return productRepository.findAll();
    }

    @Override
    public void deleteProducts() {
        productRepository.deleteAllInBatch();
    }

    public Product getProduct(Long id) {
        try {
             return productRepository.findById(id).get();
        } catch (NoSuchElementException e) {
            logger.error(e.getLocalizedMessage(), e);
            throw e;
        }
    }

    public void deleteProduct(Long id) {
        productRepository.deleteById(id);
    }

}
