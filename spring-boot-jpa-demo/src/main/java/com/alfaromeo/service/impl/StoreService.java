package com.alfaromeo.service.impl;

import com.alfaromeo.domain.LFSStore;
import com.alfaromeo.domain.ProductDetail;
import com.alfaromeo.model.Area;
import com.alfaromeo.model.City;
import com.alfaromeo.projection.ProductDetailLite;
import com.alfaromeo.projection.StoreLite;
import com.alfaromeo.repository.ProductDetailRepository;
import com.alfaromeo.repository.StoreRepository;
import com.alfaromeo.service.IStoreService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by maverick on 27/12/17.
 */
@Service
public class StoreService implements IStoreService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private StoreRepository storeRepository;

    @Autowired
    private ProductDetailRepository detailRepository;

    @Override
    public List<LFSStore> getStores() {
        return storeRepository.findAll();
    }

    @Override
    public LFSStore getStore(Long id) {
        try {
            return storeRepository.findById(id).get();
        } catch (NoSuchElementException e) {
            logger.error(e.getLocalizedMessage(), e);
            throw e;
        }
    }

    @Override
    public void deleteStore(Long id) {
        storeRepository.deleteById(id);
    }

    @Override
    public LFSStore saveStore(LFSStore store) {
        return storeRepository.save(store);
    }

    @Override
    public List<StoreLite> getStoresByArea(Area area) {
        return storeRepository.findByArea(area);
    }

    @Override
    public List<StoreLite> getStoresByCity(City city) {
        return storeRepository.findByCity(city);
    }

    @Override
    public List<StoreLite> getStoresByNearbyLocation(String locationlike) {
        return storeRepository.findByLocationLike("%"+locationlike+"%");
    }

    @Override
    public StoreLite getStoreByLocation(String location) {
        try {
            return storeRepository.findByLocation(location).get();
        } catch (NoSuchElementException e) {
            logger.error(e.getLocalizedMessage(), e);
            throw e;
        }
    }

    @Override
    public void addProductToStore(Long storeId, ProductDetail product) {
        try {
            LFSStore store = storeRepository.findById(storeId).get();
            ProductDetail storedProduct = detailRepository.save(product);
            store.getProducts().add(storedProduct);
            storeRepository.save(store);
        } catch (NoSuchElementException e) {
            logger.error(e.getLocalizedMessage(), e);
            throw e;
        }
    }

    @Override
    public void removeProductFromStore(Long storeId, Long productDetailId) {
        try {
            LFSStore store = storeRepository.findById(storeId).get();
            ProductDetail product = store.getProducts().stream()
                    .filter(productDetail -> productDetailId.equals(productDetail.getId()))
                    .findAny().get();
            store.getProducts().remove(product);
            detailRepository.delete(product);
        } catch (NoSuchElementException e) {
            logger.error(e.getLocalizedMessage(), e);
            throw e;
        }
    }

    @Override
    public List<ProductDetailLite> getProductsByDiscount(Long storeId, Double discount) {
        try {
            LFSStore store = storeRepository.findById(storeId).get();
            return detailRepository.findByStoreAndDiscount(store, discount);
        } catch (NoSuchElementException e) {
            logger.error(e.getLocalizedMessage(), e);
            throw e;
        }
    }

    @Override
    public List<ProductDetailLite> getOutOfStockProducts(Long storeId) {
        try {
            LFSStore store = storeRepository.findById(storeId).get();
            return detailRepository.findByStoreAndTotalNumberInStock(store, 0);
        } catch (NoSuchElementException e) {
            logger.error(e.getLocalizedMessage(), e);
            throw e;
        }
    }

}
