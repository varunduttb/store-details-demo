package com.alfaromeo.service.impl;

import com.alfaromeo.domain.ProductDetail;
import com.alfaromeo.repository.ProductDetailRepository;
import com.alfaromeo.service.IProductDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by maverick on 27/12/17.
 */
@Service
public class ProductDetailService implements IProductDetailService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ProductDetailRepository productDetailRepository;

    @Override
    public List<ProductDetail> getProductDetails() {
        return productDetailRepository.findAll();
    }

    @Override
    public ProductDetail getProductDetail(Long id) {
        try {
            return productDetailRepository.findById(id).get();
        } catch (NoSuchElementException e) {
            logger.error(e.getLocalizedMessage(), e);
            throw e;
        }
    }

    @Override
    public void deleteProductDetail(Long id) {
       productDetailRepository.deleteById(id);
    }

    @Override
    public ProductDetail saveProductDetail(ProductDetail productDetail) {
        return productDetailRepository.save(productDetail);
    }
}
