package com.alfaromeo.service;

import com.alfaromeo.domain.Product;

import java.util.List;

/**
 * Created by Varun on 20/12/17.
 */
public interface IProductService {

    Product getProduct(Long id);

    Product createProduct(Product person);

    void deleteProduct(Long id);

    List<Product> getProducts();

    void deleteProducts();
}
