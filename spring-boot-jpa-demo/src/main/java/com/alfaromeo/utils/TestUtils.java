package com.alfaromeo.utils;

import com.alfaromeo.common.domain.User;
import com.alfaromeo.common.repository.UserRepository;
import com.alfaromeo.domain.LFSStore;
import com.alfaromeo.domain.Product;
import com.alfaromeo.domain.ProductDetail;
import com.alfaromeo.model.Area;
import com.alfaromeo.model.City;
import com.alfaromeo.repository.ProductDetailRepository;
import com.alfaromeo.repository.ProductRepository;
import com.alfaromeo.repository.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by maverick on 24/12/17.
 */
@Component
public class TestUtils {

    @Autowired
    private ProductDetailRepository productDetailRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private StoreRepository storeRepository;

    @Autowired
    private UserRepository userRepository;

    public List<Product> getProducts() {
        Product p1 = new Product("Fastrack-sm01", "123-334-112", "Fastrack smart watch with heart rate sensor" +
                ", pedometer and sleep monitoring", 1350.89);
        Product p2 = new Product("Fastrack-sg01", "er2-de1-dfr", "Fastrack sunglasses with UV protection", 12000.00);
        Product p3 = new Product("Fastrack-eg01", "12c-err-rrt", "Fastrack eye-lens with anti-glare capability", 456.8);
        List<Product> ps = Stream.of(p1, p2, p3).collect(Collectors.toList());
        productRepository.saveAll(ps);
        return ps;
    }

    public List<ProductDetail> getProductDetails() {
        List<Product> productDetails = productRepository.findAll();
        ProductDetail detail1 = new ProductDetail(productDetails.get(0), 73.0, 200);
        ProductDetail detail2 = new ProductDetail(productDetails.get(1), 62.0, 430);
        ProductDetail detail3 = new ProductDetail(productDetails.get(2), 53.0, 600);
        List<ProductDetail> detailList = Stream.of(detail1, detail2, detail3).collect(Collectors.toList());
        productDetailRepository.saveAll(detailList);
        return detailList;
    }

    public List<LFSStore> getStores() {
        List<User> users = userRepository.findAll();
        List<User> cros1 = Stream.of(users.get(0)).collect(Collectors.toList());//{"John", "Doe"};
        List<User> cros2 = Stream.of(users.get(1)).collect(Collectors.toList());//{"Max", "Dex"};
        List<User> cros3 = Stream.of(users.get(2)).collect(Collectors.toList());//{"Mike", "Sam"};
        List<ProductDetail> productDetails = productDetailRepository.findAll();
        LFSStore store1 = new LFSStore("someplaceInBangalore", "41°24'12.2\"N 2°10'26.5\"E",
                Stream.of(productDetails.get(0)).collect(Collectors.toList())
                , cros1, Area.SOUTH, City.BENGALURU);
        LFSStore store2 = new LFSStore("someplaceInChennai", "42°24'12.2\"N 2°10'26.5\"E",
                Stream.of(productDetails.get(1)).collect(Collectors.toList()),
                cros2, Area.SOUTH, City.CHENNAI);
        LFSStore store3 = new LFSStore("someplaceInMumbai", "43°24'12.2\"N 2°10'26.5\"E",
                Stream.of(productDetails.get(2)).collect(Collectors.toList()),
                cros3, Area.WEST, City.MUMBAI);
        List<LFSStore> lfsStores = Stream.of(store1, store2, store3).collect(Collectors.toList());
        storeRepository.saveAll(lfsStores);
        return lfsStores;
    }

    @PostConstruct
    public void init() {
        SecurityContextHolder.getContext().setAuthentication(new Authentication() {
            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                return Stream.of(new SimpleGrantedAuthority("ADMIN"))
                        .collect(Collectors.toList());
            }

            @Override
            public Object getCredentials() {
                return null;
            }

            @Override
            public Object getDetails() {
                return null;
            }

            @Override
            public Object getPrincipal() {
                return "lewis";
            }

            @Override
            public boolean isAuthenticated() {
                return true;
            }

            @Override
            public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
            }

            @Override
            public String getName() {
                return "lewis";
            }
        });
        getProducts();
        getProductDetails();
        getStores();
    }
}
